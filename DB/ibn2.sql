-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 07, 2017 at 01:24 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibn2`
--

-- --------------------------------------------------------

--
-- Table structure for table `g_users`
--

CREATE TABLE `g_users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `g_users`
--

INSERT INTO `g_users` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `link`, `created`, `modified`) VALUES
(1, 'google', '112601884794712941050', 'Amrinder', 'Malhi', 'amalhi2@gmail.com', 'female', 'en', 'https://lh3.googleusercontent.com/-h81x4u-NuDY/AAAAAAAAAAI/AAAAAAAAAZE/D4dnrSKqLDs/photo.jpg', 'https://plus.google.com/112601884794712941050', '2017-05-13 20:05:29', '2017-05-14 18:21:22'),
(2, 'google', '107432600236844015211', 'Matthew', 'Perkovic', 'noipmatt@gmail.com', '', 'en-GB', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'https://plus.google.com/107432600236844015211', '2017-07-14 07:26:46', '2017-08-15 05:11:41'),
(5, '', '', '', '', '', '', '', '', '', '2017-07-26 00:00:00', '2017-07-31 00:00:00'),
(6, '', '', '', '', '', '', '', '', '', '2017-07-26 00:00:00', '2017-07-31 00:00:00'),
(7, 'google', '101462498625038602253', 'Matthew', 'Grieve', 'matthewgrieve472@gmail.com', NULL, 'en', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', '2017-07-28 03:25:33', '2017-08-01 23:58:26'),
(8, 'google', '113595832144571646322', 'Harshit', 'Singhal', 'watsontopper@gmail.com', NULL, 'en', 'https://lh6.googleusercontent.com/-bFdxUuRffLQ/AAAAAAAAAAI/AAAAAAAAAsk/jN2Tb7b997M/photo.jpg', 'https://plus.google.com/113595832144571646322', '2017-07-28 06:59:28', '2017-07-28 06:59:58'),
(9, 'google', '113439649845902690922', 'Jo', 'Martin', 'joanne.martin.au@gmail.com', NULL, 'en', 'https://lh3.googleusercontent.com/-mg2DReYIKzU/AAAAAAAAAAI/AAAAAAAAAQo/uw9l7qfazv4/photo.jpg', 'https://plus.google.com/113439649845902690922', '2017-07-31 04:52:26', '2017-07-31 04:52:47'),
(10, 'google', '111171712764741103401', 'Harshit', 'Singhal', 'hpsinghal17@gmail.com', NULL, 'en', 'https://lh4.googleusercontent.com/-9mzDp_uubEs/AAAAAAAAAAI/AAAAAAAAAMY/b-ZJwzzYJ6k/photo.jpg', 'https://plus.google.com/111171712764741103401', '2017-07-31 04:53:46', '2017-07-31 04:53:47');

-- --------------------------------------------------------

--
-- Table structure for table `l_users`
--

CREATE TABLE `l_users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `l_users`
--

INSERT INTO `l_users` (`id`, `oauth_provider`, `oauth_uid`, `fname`, `lname`, `email`, `location`, `country`, `picture_url`, `profile_url`, `created`, `modified`) VALUES
(1, 'linkedin', '8T2N6bkp-S', 'Matthew Perkovic', 'Perkovic', 'matthew_1246@hotmail.com', 'Sydney, Australia', 'au', 'https://media.licdn.com/mpr/mprx/0_Ow-uHBsVtmtmLqOop7z1Hzw5rELG5lOopd61HqVLfWA-szdEtSG8QNmIOG5j6n060ertFPHzv60t', 'https://www.linkedin.com/in/matthew-perkovic-perkovic-39690949', '2017-07-14 08:02:11', '2017-07-20 02:38:05'),
(2, 'linkedin', 'StdWE5zcOF', 'Matthew Perkovic', 'Perkovic', 'matthew_1246@hotmail.com', 'Sydney, Australia', 'au', 'https://media.licdn.com/mpr/mprx/0_Ow-uHBsVtmtmLqOop7z1Hzw5rELG5lOopd61HqVLfWA-szdEtSG8QNmIOG5j6n060ertFPHzv60t', 'https://www.linkedin.com/in/matthew-perkovic-perkovic-39690949', '2017-07-18 03:31:20', '2017-07-28 04:09:14'),
(3, 'linkedin', 'pmd7qhnbE4', 'Matthew', 'Perkovic', 'noipmatt@gmail.com', 'Sydney, Australia', 'au', '', 'https://www.linkedin.com/in/matthew-perkovic-69a44a98', '2017-07-27 04:08:29', '2017-07-28 04:08:46'),
(4, 'linkedin', '7oT2yxDR8m', 'Intelligent Business', 'Networks', 'info@ibznets.com', 'Sydney, Australia', 'au', 'https://media.licdn.com/mpr/mprx/0_1KFATv-rPVei-1jW03PKEbqrt25i-tjkxKxAkT7rta5C-CfzKNjp7XZrrWeic3pW1NlxWGBKRoZGqnOcsiGDWTVyYoZ_qnw5siG-C8flvISfO8KJ9tNyGq38OxCPgnpN18QjLYUBwOt', 'https://www.linkedin.com/in/ibznets', '2017-07-29 00:09:27', '2017-07-29 00:27:17'),
(5, 'linkedin', 'Px2RUwR9Vp', 'Matthew Perkovic', 'Perkovic', 'matthew_1246@hotmail.com', 'Sydney, Australia', 'au', 'https://media.licdn.com/mpr/mprx/0_Ow-uHBsVtmtmLqOop7z1Hzw5rELG5lOopd61HqVLfWA-szdEtSG8QNmIOG5j6n060ertFPHzv60t', 'https://www.linkedin.com/in/matthew-perkovic-perkovic-39690949', '2017-07-29 00:11:05', '2017-07-30 23:54:41'),
(6, 'linkedin', 'IMPZLr62nk', 'Matthew', 'Perkovic', 'noipmatt@gmail.com', 'Sydney, Australia', 'au', '', 'https://www.linkedin.com/in/matthew-perkovic-69a44a98', '2017-07-29 00:11:43', '2017-07-30 23:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `Firstname` varchar(100) DEFAULT NULL,
  `Lastname` varchar(100) DEFAULT NULL,
  `Email` varchar(100) NOT NULL DEFAULT '',
  `Password` varchar(100) DEFAULT NULL,
  `interested_in` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`Firstname`, `Lastname`, `Email`, `Password`, `interested_in`) VALUES
('khbv', 'gh j', 'a.bose@gg.com', 'dd5e2237b84ece0f59d45f3986914942', NULL),
('asd', 'asd', 'a@g.com', 'dd5e2237b84ece0f59d45f3986914942', NULL),
('Abhinav', 'Bose', 'abhinav.bose13@yahoo.in', '95ff7345feb785339d4b666ca3e285db', NULL),
('Rajiv', 'Punia', 'r.punia@ibznets.com', 'eb5dee8fd51d7eefe4b5cbdde7ba31cf', NULL),
('bose', 'abhinav', 'aa@g.com', '22639b7b3d126a5aea919ce3c58b4224', NULL),
('abhinav', 'bose', 'abhi@gmail.com', 'dd5e2237b84ece0f59d45f3986914942', NULL),
('A', 'M', 'malhi_amrinder@yahoo.com', '532a8898645e085c968bfff3963f1329', NULL),
('Matthew', NULL, 'matthew1246@hotmail.com', NULL, NULL),
('Matthew', NULL, 'matthew1246@hotmail.com', NULL, NULL),
('Matthew', NULL, '', NULL, NULL),
('', NULL, 'matthew1246@hotmail.com', NULL, NULL),
('Matthew', 'Smith', 'user@gmail.com', 'password', NULL),
('Lisa', 'Apple', 'kjsdfjk@hotmail.com', 'ae2b1fca515949e5d54fb22b8ed95575', NULL),
('James', 'Bond', 'james@jksdfjk.com', '365d38c60c4e98ca5ca6dbc02d396e53', NULL),
('Matthew', 'Perkovic', '', 'f5d1278e8109edd94e1e4197e04873b9', NULL),
('Matthew', '', 'Matthew@hotmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL),
('', 'Perkovic', 'jksdfkjf@hotmail.com', 'd5d630d4355544115ee3ade77a6141ee', NULL),
('Matthew', 'Perkovic', 'matthew_1246@hotmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL),
('Matthew', 'Perkovic', 'matthew1246@hotmail.com', 'Password', 'Launching Product'),
('Matthew', 'Perkovic', 'test@email.com', '63a9f0ea7bb98050796b649e85481845', 'LaunchingProduct'),
('Matthew', 'Perkovic', 'test@email.com', '63a9f0ea7bb98050796b649e85481845', 'LaunchingProduct'),
('Matthew', 'Perkovic', 'test@email.com', '63a9f0ea7bb98050796b649e85481845', 'LaunchingProduct'),
('Matthew', 'Ikdfjkdjk', 'root@hotmail.com', '63a9f0ea7bb98050796b649e85481845', 'DiscountedProducts'),
('Matthew', 'Perkovic', 'root@djsffkjf.com', '63a9f0ea7bb98050796b649e85481845', ''),
('James', 'Geoff', 'ksdjfjk@hotmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', ''),
('sjdfkjdf', 'dkjfkjdsfjk', 'kjsdfjkf@hotmail.com', 'eccfd7f5ece92e8905e011fe80f4ae84', ''),
('List', 'Lastname', 'ffjkdkjf@hofdjkdf.com', '432bb94a14b8184bb00a3be5b4d510a9', ''),
('fjdjkf', 'kjdfkjfsdjk', 'kjdfkjfj@hotmail.com', 'dd931c3bc789b2bcfdb92624bd5ab86c', ''),
('skjfdfjk', 'kjdjkjk', 'jkdsfkjsdkjlf@hotmail.com', '7469de7dfd3ca7827345c452bd8d132d', ''),
('jsdfjksdf', 'kjfdjkjkf', 'sdkjkfjsdfkj@hotmail.com', '6983c826e8b26782b9ea76c5fe47b548', ''),
('JKjkfdkjf', 'kjdjkfjkdf', 'djkfkjd@hotmail.com', '50a85afd40ffa46017d4210875aea1fe', 'LaunchingProduct'),
('KJfkjdfjk', 'KJFdkjfjkf', 'dsjkfkjdfkjdf@hotmail.com', '3bad7e23473d7e9d0a14b76c2c35fd50', 'LaunchingProduct'),
('KJFsdjkfjk', 'kjsdfjkkjdsf', 'kjdkjfdsjkf@hotmail.com', '69c17e1fe70cc3f51531ad72e7f6630b', 'four'),
('Matthew', '', 'sjkdfkjdf@hotmail.com', '0df2bd9cf1087534fd545f387db0a37c', ' Online;'),
('Matthew', 'Perkovic', 'sdkjfkjdfjk@hotmail.com', '6d05494c66ea091a04a9e7f87c0839bd', ' 1; 2; 3; 4; 5;'),
('', '', '', 'd41d8cd98f00b204e9800998ecf8427e', ' 1; 2; 3; 4; 5;'),
('', '', '', 'd41d8cd98f00b204e9800998ecf8427e', ''),
('', '', '', 'd41d8cd98f00b204e9800998ecf8427e', ''),
('Matthew', 'Perkovic', 'noipmatt@gmail.com', 'cb1c3d7656e906f9f09553887033dbbf', ' 1; 2; 3; 4; 5;'),
('Harshit', 'Singhal ', 'watsontopper@gmail.com', '5692a15f4ca429f8853a829c169671d1', ''),
('Matthew', 'Kfjsdkjfjkd', 'noipmatt@gmail.com', 'e4ad82f38bc973c03935d57c98e6c86f', ''),
('Jfjkdkfj', 'sdsdkjfjkf', 'noipmatt@gmail.com', '0b0c46f149b8ec53e39fa72885ee0345', ''),
('FJKdsjkf', 'Iksdfjksdfkj', 'noipmatt@gmail.com', 'bb81b14d3bf40fdd139a5a72113036a6', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_users`
--

CREATE TABLE `t_users` (
  `id` int(11) NOT NULL,
  `oauth_provider` enum('','facebook','google','twitter','linkedin') COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_users`
--

INSERT INTO `t_users` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `username`, `link`, `created`, `modified`) VALUES
(0, 'twitter', '8.6189782322221E+17', 'Amrinder', '', '', '', 'en', 'http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png', 'AmrinderMalhi42', 'https://twitter.com/AmrinderMalhi42', '2017-05-16 15:59:28', '2017-05-16 16:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `oauth_provider` enum('','facebook','google','twitter') COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `link`, `created`, `modified`) VALUES
(1, 'facebook', '10213144401710045', 'Amrinder', 'Kaur', 'amalhi2@gmail.com', 'femal', 'en_US', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/17457857_10212813287832405_5982194004152722093_n.jpg?oh=bdb091195978c922a3f717922d9aa02e&oe=59B9AE02', 'https://www.facebook.com/app_scoped_user_id/10213144401710045/', '2017-04-30 16:36:23', '2017-05-04 18:42:16'),
(2, 'facebook', '10213276340088422', 'Amrinder', 'Kaur', 'amalhi2@gmail.com', 'femal', 'en_US', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/18447442_10213281821825462_4656174999687314220_n.jpg?oh=afb019bf0fc47769a059f2df57c493fa&oe=597A05D9', 'https://www.facebook.com/app_scoped_user_id/10213276340088422/', '2017-05-13 18:14:51', '2017-05-14 18:15:48'),
(3, 'facebook', '10210105769380901', 'Matthew', 'Perkovic', 'matthew_1246@hotmail.com', 'male', 'en_US', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c154.33.412.412/s50x50/247380_1761790564718_6824706_n.jpg?oh=d9619a655f6380155c8f7916a1cc3a3e&oe=59FE00A6', 'https://www.facebook.com/app_scoped_user_id/10210105769380901/', '2017-07-14 06:54:30', '2017-07-26 08:37:36'),
(4, '', '', 'Matthew', 'Perkovic', 'matthew1246@hotmail.com', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '', '', 'Matthew', 'Perkovic', 'matthew1246@hotmail.com', '', '', '', '', '2017-07-17 00:00:00', '2017-07-17 00:00:00'),
(6, '', '', 'Matthew', 'Smith', 'matthew1246@hotmail.com', '', '', '', '', '2017-07-17 04:05:05', '2017-07-17 04:05:05'),
(7, '', '', 'James', 'Grieve', 'masdjksdf@hotmail.com', '', '', '', '', '2017-07-17 04:25:56', '2017-07-17 04:25:56'),
(8, '', '', '', '', '', '', '', '', '', '2017-07-19 05:52:12', '2017-07-19 05:52:12'),
(9, '', '', 'Matthew', 'Perkovic', 'noipmatt@hotmail.com', '', '', '', '', '2017-07-20 01:32:38', '2017-07-20 01:32:38'),
(10, 'facebook', '10210157759880631', 'Matthew', 'Perkovic', 'matthew_1246@hotmail.com', 'male', 'en_US', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c154.33.412.412/s50x50/247380_1761790564718_6824706_n.jpg?oh=d9619a655f6380155c8f7916a1cc3a3e&oe=59FE00A6', 'https://www.facebook.com/app_scoped_user_id/10210157759880631/', '2017-07-20 01:42:41', '2017-07-20 01:46:33'),
(11, '', '', 'Matthew', 'Perkovic', 'matthew1246@hotmail.com', '', '', '', '', '2017-07-20 02:06:36', '2017-07-20 02:06:36'),
(12, '', '', 'Matthew', 'Perkovic', 'matthew1246@hotmail.com', '', '', '', '', '2017-07-20 02:06:58', '2017-07-20 02:06:58'),
(13, '', '', '', '', '', '', '', '', '', '2017-07-20 02:46:40', '2017-07-20 02:46:40'),
(14, '', '', '', '', '', '', '', '', '', '2017-07-20 02:48:38', '2017-07-20 02:48:38'),
(15, 'facebook', '1936227059923709', 'Launch', 'Pad', 'rajivpunia@y7mail.com', 'male', 'en_GB', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c172.37.457.457/s50x50/426675_1375355756010845_1566195380_n.jpg?oh=c08f1df36da19947b163050221c958d9&oe=5A5A30CD', 'https://www.facebook.com/app_scoped_user_id/1936227059923709/', '2017-07-27 23:50:24', '2017-08-31 04:27:34'),
(16, 'facebook', '10210227271698383', 'Matthew', 'Perkovic', 'matthew_1246@hotmail.com', 'male', 'en_US', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c154.33.412.412/s50x50/247380_1761790564718_6824706_n.jpg?oh=a62c1e854faf1532296639279260670a&oe=5A258DA6', 'https://www.facebook.com/app_scoped_user_id/10210227271698383/', '2017-07-27 23:59:48', '2017-08-30 01:07:56'),
(17, 'facebook', '1456095027759967', 'Harshit', 'Singhal', 'watsontopper@gmail.com', 'male', 'en_US', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c0.0.50.50/p50x50/12243343_965142360188572_973540361110017736_n.jpg?oh=f198f6a4af60d2cb7416cec77475a0a3&oe=5A304AD5', 'https://www.facebook.com/app_scoped_user_id/1456095027759967/', '2017-07-31 04:53:25', '2017-08-15 03:56:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `g_users`
--
ALTER TABLE `g_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `l_users`
--
ALTER TABLE `l_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `g_users`
--
ALTER TABLE `g_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `l_users`
--
ALTER TABLE `l_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
