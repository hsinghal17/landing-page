/*
Disabling scroll of the the website
*/

// $("body").addClass("stop");


/*
Function for randomly selecting images on the homepage
*/

// function selectStep() {
//     var number = Math.floor(Math.random() * 3) + 1;
//     if (number == 1) {
//         $(".PictureGallery").css("background-image", "url('1.jpg')");
//     } else if (number == 2) {
//         $(".PictureGallery").css("background-image", "url('2.jpg')");
//     } else if (number == 3) {
//         $(".PictureGallery").css("background-image", "url('3.jpg')");
//     }
// }
/*
Code that captures the screen size and adjust the cascase style sheet
accordingly
return = NULL;
*/

// NOTE to check
// console.log("Hello, I am still Alive");

//creating the isEven function in order to toggle the button


$(".RegisterForm").css("visibility", "hidden");
$(".ContactUs").css("visibility", "hidden");
// $(".MenuButton").hide();
$(".navigation").css("transform-origin", "center center");

console.log("Hello, I am still alive");


function toggle_menu_button() {
  $(".navigation").css("font-size", "100%");
  $(".Two").css("height", "0%");
  $(".One").css("margin-top", "32%");
  $(".Two").css("margin-top", "-32%");
  $(".One").css("transform", "rotate(135deg)");
  $(".Three").css("transform", "rotate(-135deg)");
}

var windowWidth = $(window).width();
var windowHeight =$(window).height();

function show_ContactUs_Form() {
  $(".ContactUs").css("visibility", "visible");
    $(".ContactUs").animate({
      width: "100%",
      height: "100vh"
    }, 400);
}

function hide_ContactUs_Form() {
  $(".ContactUs").animate({
    width: "0%",
    height: "0vh"
  }, 400);
  $(".ContactUs").css("visibility", "hidden");
}

function show_Registration_Form() {
  $(".RegisterForm").css("visibility", "visible");
  $(".DropDownMenuOptions").css("visibility", "visible");
  $(".RegisterForm").animate({
    width: "100%",
    height: "100vh"
  }, 400);
}


function hide_Registration_Form() {
  $(".RegisterForm").animate({
    width: "0%",
    height: "0vh"
  }, 400);
  $(".RegisterForm").css("visibility", "hidden");
  $(".DropDownMenuOptions").css("visibility", "hidden");
}



//Hide division on key code
$(document).on('keydown', function(e) {
  if (e.keyCode === 27) { // ESC
    hide_Registration_Form();
    hide_ContactUs_Form();
  }
});
