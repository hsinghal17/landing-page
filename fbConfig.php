<?php
if(!session_id()){
    session_start();
}

// Include the autoloader provided in the SDK
require_once __DIR__ . '/facebook-php-sdk/autoload.php';

// Include required libraries
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
//$_SESSION["SR"] = $_SERVER['SERVER_NAME'];
/*
 * Configuration and setup Facebook SDK
 */
$appId         = '1919544694972962'; //Facebook App ID
$appSecret     = 'ef56eccb78900b26c1158e6b00b84a67'; //Facebook App Secret
$redirectURL   = 'http://www.ibznets.com/';
//$redirectURL = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$fbPermissions = array('email');  //Optional permissions
//file_put_contents('output.txt',PHP_EOL . PHP_EOL . 'Hello World!',FILE_APPEND);

$fb = new Facebook(array(
    'app_id' => $appId,
    'app_secret' => $appSecret,
    'default_graph_version' => 'v2.2',
));

// Get redirect login helper
$helper = $fb->getRedirectLoginHelper();

// Try to get access token
try {
    if(isset($_SESSION['facebook_access_token'])){
        $accessToken = $_SESSION['facebook_access_token'];
    }else{
          $accessToken = $helper->getAccessToken();
    }
} catch(FacebookResponseException $e) {
     echo 'Graph returned an error: ' . $e->getMessage();
      exit;
} catch(FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
      //exit;
}

?>
