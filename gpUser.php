<?php
class gpUser {
	private $dbHost     = "localhost";
    private $dbUsername = "root";
    private $dbPassword = "My1pass";
    private $dbName     = "ibn2";
    private $userTbl    = "g_users";
	
	function __construct(){
        if(!isset($this->db)){
            // Connect to the database
            $conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
            if($conn->connect_error){
                die("Failed to connect with MySQL: " . $conn->connect_error);
            }else{
                $this->db = $conn;
            }
        }
    }
	
	function checkUser($userData = array()){
        if(!empty($userData)){
            //print_r("not empty two: ");
            //print_r($userData);
            //Check whether user data already exists in database
            $prevQuery = "SELECT * FROM ".$this->userTbl." WHERE oauth_provider = '".$userData['oauth_provider'].
            "' AND oauth_uid = '".$userData['oauth_uid']."'";
            $prevResult = $this->db->query($prevQuery);
            //print_r("prevResult: ");
            //print_r($prevResult);
            if($prevResult->num_rows > 0){
                //Update user data if already exists
                $query = "UPDATE ".$this->userTbl." SET first_name = '".$userData['first_name']."', last_name = '".$userData['last_name']."', email = '".$userData['email']."', locale = '".$userData['locale']."', picture = '".$userData['picture']."', link = '".$userData['link']."', modified = '".date("Y-m-d H:i:s")."' WHERE oauth_provider = '".$userData['oauth_provider']."' AND oauth_uid = '".$userData['oauth_uid']."'";
                $update = $this->db->query($query);
            }else{
                echo "userData['link'] : ";
                //print_r($userData['link']);
                //print_r("!zero rows!");
                //Insert user data
                $query = "INSERT INTO ".$this->userTbl." SET oauth_provider = '".$userData['oauth_provider'].
                "', oauth_uid = '".$userData['oauth_uid']."', first_name = '".$userData['first_name'].
                "', last_name = '".$userData['last_name'].
                "', email = '".$userData['email']."', locale = '".$userData['locale'].
                "', picture = '".$userData['picture']."', link = '".$userData['link'].
                "', created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."'";
                //echo 'Long query: ';
                //print_r($query);
                $insert = $this->db->query($query);
                //echo "Type: ";
                //print_r(gettype($insert));
                /*echo "Query: ";
                if($insert === TRUE)
                {
                     echo 'TRUE';
                }
                else
                {
                     echo 'FALSE';
                }*/
            }
            
            //Get user data from the database
            $result = $this->db->query($prevQuery);
            /*print_r("result is: ");
            print_r($result);*/
            $userData = $result->fetch_assoc();
            /*print_r("three: ");
            print_r($userData);*/
        }
        //Return user data
        return $userData;
    }
}
?>
