<?php
include "connectit.php";
if (isset($_SESSION['lblms'])) {
    echo $_SESSION['lblms'];
    unset($_SESSION['lblms']);
}
$top_right_hand_corner_output = '<a href="#" onclick="show_Registration_Form();" return false;>Register</a>';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['register_form'])) {
        //file_put_contents('outputv2.txt', PHP_EOL . PHP_EOL . print_r($_POST,true),FILE_APPEND);
        $Full_Name = '';
        $First_Name    = '';
        $Last_Name     = '';
        $Email_Address = '';
        $checkbox      = '';
        $password      = '';
        if(isset($_POST['Full_Name']))
        {
          $Full_Name = $_POST['Full_Name'];
          $array = explode(' ', $Full_Name);
          //print_r($array);
          if(count($array) === 2)
          {
            $First_Name = $array[0];
            $Last_Name = $array[1];
          }
          else if(count($array) === 1)
          {
            $First_Name = $array[0];
          }
          else if(count($array) > 2)
          {
            $First_Name = $array[0];
            $str = '';
            for($x = 1; $x < (count($array) - 1);$x++)
            {
              $str = $str . $array[$x] . ' ';
            }
            $str = $str . $array[(count($array) - 1)];
            $Last_Name = $str;
          }
        }
        if (isset($_POST['Email_Id'])) {
            $Email_Address = $_POST['Email_Id'];
        }
        if (isset($_POST['password'])) {
            $password = $_POST['password'];
        }
        if (isset($_POST['VisiterType'])) {
            //print_r($_POST['VisiterType']);
            for ($i = 0; $i < count($_POST['VisiterType']); $i++) {
                $checkbox = $checkbox . " " . $_POST['VisiterType'][$i];
            }
            //echo $checkbox;
        }

        $secretKey = '6LfIfioUAAAAAKrc1BZPqB1x_1tZthyOcawZOjHh';
        $captcha = "";
        if(isset($_POST['g-recaptcha-response']))
        {
          $captcha = $_POST['g-recaptcha-response'];
        }
        else
        {
          $captcha = "";
        }
        $ip = $_SERVER['REMOTE_ADDR'];
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        if($responseKeys['success'] === FALSE)
        {
          die("No Recaptcha at all!");
        }


        $First_Name = $dbLINK->real_escape_string($First_Name);
        $Last_Name = $dbLINK->real_escape_string($Last_Name);
        $Email_Address = $dbLINK->real_escape_string($Email_Address);
        $password = $dbLINK->real_escape_string($password);
        $checkbox = $dbLINK->real_escape_string($checkbox);
        $result_from_query = $dbLINK->query("INSERT INTO `ibn2`.`signup` (`Firstname`, `Lastname`, `Email`, `Password`, `interested_in`) VALUES ('" . $First_Name . "', '" . $Last_Name . "', '" . $Email_Address . "', md5('" . $password . "'), '" . $checkbox . "');");
        if ($result_from_query === TRUE) {
            $top_right_hand_corner_output = '<a href="#">Thank you for registering.</a>';
        } else {
            echo $dbLINK->error;
            die;
        }
    }
}
//echo $top_right_hand_corner_output;
if (isset($Insert)) {
    file_put_contents('output.txt','$Insert is called.',FILE_APPEND);
    echo "Test OK";
    $Firstname = mysqli_escape_string($dbLINK,$Firstname);
    $Lastname = mysqli_escape_string($dbLINK,$Lastname);
    $Email = mysqli_escape_string($dbLINK,$Email);
    $C_Password = mysqli_escape_string($dbLINK,$C_Password);
    $query = mysqli_query($dbLINK, "insert into signup value('$Firstname','$Lastname','$Email',md5('$C_Password'))");
    if ($query) {

        //include("email.php");
        header("Location:index.php");
    } else {

        echo mysqli_error($dbLINK);
    }
}

/* Test LinkedIn */
$linkedin_output = '';
$linkedin_style = '';
if (isset($_SESSION['loggedin'])) {
    //$linkedin_output = '<a href="logout.php"  class="btn btn-primary" style="margin-bottom:20px;padding-left:75px;padding-right:75px;">Logout</a>';
      $linkedin_output = 'logout.php';
      $linkedin_style = "background-image: url('LogoutOfLinkedin.svg');";

} else //if not logged in
    {
    $linkedin_output = 'process.php';
    $linkedin_style = "background-image: url('LoginWithLinlkedIn.svg');";
}
$output_style = '';
$gpoutput_style = '';
//file_put_contents("output.txt",PHP_EOL . $linkedin_output . "",FILE_APPEND);
/* End Test LinkedIn */

/* Facebook code */
require_once 'fbConfig.php';
require_once 'User.php';

if (isset($accessToken)) {
    if (isset($_SESSION['facebook_access_token'])) {
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    } else {
        // Put short-lived access token in session
        $_SESSION['facebook_access_token'] = (string) $accessToken;

        // OAuth 2.0 client handler helps to manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Exchanges a short-lived access token for a long-lived one
        $longLivedAccessToken              = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

        // Set default access token to be used in script
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }

    // Redirect the user back to the same page if url has "code" parameter in query string
    if (isset($_GET['code'])) {
        header('Location: ./');
    }

    // Getting user facebook profile info
    try {
        $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
        $fbUserProfile  = $profileRequest->getGraphNode()->asArray();
    }
    catch (FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        session_destroy();
        // Redirect user back to app login page
        header("Location: ./");
        exit;
    }
    catch (FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    // Initialize User class
    $user = new User();

    // Insert or update user data to the database
    $fbUserData = array(
        'oauth_provider' => 'facebook',
        'oauth_uid' => $fbUserProfile['id'],
        'first_name' => $fbUserProfile['first_name'],
        'last_name' => $fbUserProfile['last_name'],
        'email' => $fbUserProfile['email'],
        'gender' => $fbUserProfile['gender'],
        'locale' => $fbUserProfile['locale'],
        'picture' => $fbUserProfile['picture']['url'],
        'link' => $fbUserProfile['link']
    );
    $userData   = $user->checkUser($fbUserData);

    $_SESSION["fbemail"]  = $userData['email'];
    // Put user data into session
    $_SESSION['userData'] = $userData;

    // Get logout url
    $logoutURL = $helper->getLogoutUrl($accessToken, $redirectURL . 'logout.php');

    // Render facebook profile data
    if (!empty($userData)) {

        //$output = '<br/>';

        //$output .= '<br/><a href="'.$logoutURL.'"  class="btn btn-primary" style="margin-bottom:20px;padding-left:75px;padding-right:75px;" >Logout</a> ';

        //$output = '<a href="' . $logoutURL . '"  class="btn btn-primary" style="margin-bottom:20px;padding-left:75px;padding-right:75px;" >Logout</a> ';
        $output =  $logoutURL;
        $output_style = "background-image: url('LogoutOfFacebook.svg');";

    } else {
        $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
    }

} else {
    // Get login url
    $loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);

    // Render facebook login button
    //$output = '<a href="' . htmlspecialchars($loginURL) . '"><img src="images/fblogin-btn.png" style="height:80px;width:230px;"></a>';
    $output = $loginURL;
    $output_style = "background-image: url('LoginWithFacebook.svg');";
}
/* End Facebook code. */


//Include GP config file && User class

include_once 'gpConfig.php';
include_once 'gpUser.php';

if (isset($_GET['code'])) {
    $gClient->authenticate($_GET['code']);
    $_SESSION['token'] = $gClient->getAccessToken();
    header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
    $gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) {
    //Get user profile data from google
    $gpUserProfile = $google_oauthV2->userinfo->get();

    //Initialize User class
    $user = new gpUser();

    //Insert or update user data to the database
    $gpUserData           = array(
        'oauth_provider' => 'google',
        'oauth_uid' => $gpUserProfile['id'],
        'first_name' => $gpUserProfile['given_name'],
        'last_name' => $gpUserProfile['family_name'],
        'email' => $gpUserProfile['email'],

        'locale' => $gpUserProfile['locale'],
        'picture' => $gpUserProfile['picture'],
        'link' => $gpUserProfile['link']
    );
    //print_r("gpUserData is: ");
    //print_r($gpUserData);
    $userData             = $user->checkUser($gpUserData);
    //print_r("userData is: ");
    //print_r($userData);
    $_SESSION["fbemail"]  = $userData['email'];
    //Storing user data into session
    $_SESSION['userData'] = $userData;
    //print_r($userData);
    //Render facebook profile data
    if (!empty($userData)) {
        //$gpoutput             = '<a  class="btn btn-primary" style="padding-left:78px;padding-right:78px;" href="logout.php" > Logout</a>';
        $gpoutput             = "logout.php";
        $_SESSION["gpoutput"] = $gpoutput;
        $gpoutput_style = "background-image: url('LogoutofGoogle.svg');";
    } else {
        $gpoutput             = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
        $_SESSION["gpoutput"] = $gpoutput;
    }
} else {
    $authUrl              = $gClient->createAuthUrl();
    //$gpoutput             = '<a name="gp" href="' . filter_var($authUrl, FILTER_SANITIZE_URL) . '"><img src="images/glogin.png" style="height:35px;width:200px;" alt=""/></a>';
    $gpoutput             = filter_var($authUrl, FILTER_SANITIZE_URL);
    $_SESSION["gpoutput"] = $gpoutput;
    $gpoutput_style = "background-image: url('LoginWithGoogle.svg');";
}
?>

<!-- NOTE
  HTML Script Starts here -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="ETYk-A8SBERVio-MJIPI3JmTw3Tclo9LjaJiGyrlDVU" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IBN: Enabling Business Innovations</title>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" />
    <!-- RESET CSS -->
    <link rel="stylesheet" href="Reset.css" type="text/css" />
    <!-- STYLE SHEET -->
    <link rel="stylesheet" href="style.css" type="text/css" />
    <!-- WEBSITE ICON LINK-->
    <link rel="icon" href="Icon.png">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.css" type="text/css">
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="JS.js"></script>
    <script src="javascript/codeforapi.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
    <script src="javascript/registerformvalidation.js"></script>
</head>

<body class = "stop">
    <header>
      <a href="index.php">
      <div class="Logo"></div>
      </a>
      <div class="MenuButton" onclick="toggle_menu_button()">
        <div class="Bar One"></div>
        <div class="Bar Two"></div>
        <div class="Bar Three"></div>
      </div>
      <nav class="navigation">
          <a href="#" onclick="show_ContactUs_Form();" return false;>Contact Us</a>
          <?= $top_right_hand_corner_output ?>
      </nav>
    </header>
    <div id="VideoBG">
        <video width="320" height="240" autoplay="true" loop>
        <source src="CoverVideoForIBNLandingPage.mp4" type="video/mp4">
        <source src="CoverVideoForIBNLandingPage.webm" type="video/webm">
      </video>
    </div>
    <div class="VideoAlternative"></div>
    <div class="PictureGallery">
    </div>
      <div class="RegisterForm">
        <div class="CloseButton" onclick="hide_Registration_Form()"></div>
        <form id="selectform" class="Registeration" action="index.php" method="post">
          <input placeholder="Full Name" type="text" name="Full Name">
          <input id="filled_email_check" placeholder="Email" type="email" name="Email Id">
          <input id="filled_password_check" placeholder="Password" type="password" name="password" onChange="return checkFields(this);">
          <div class="inputInfo DropDownMenu">
            <div class="DropDownMenuTab">
              <p>Tell us about your interests</p>
            </div>
            <div class="DropDownMenuOptions">
              <input type="checkbox" name="VisiterType[]" value="1;"> <h1>Launching new product/service</h1> <br>
              <input type="checkbox" name="VisiterType[]" value="2;">  <h1>Create an online shopfront / website for your business</h1><br>
              <input type="checkbox" name="VisiterType[]" value="3;">  <h1>Looking for innovative products, amazing discounts or professional connections</h1><br>
              <input type="checkbox" name="VisiterType[]" value="4;">  <h1>Freelance Professional work</h1> <br>
              <input type="checkbox" name="VisiterType[]" value="5;">   <h1>Angel/investor looking to engage with interesting businesses</h1> <br>
            </div>
          </div>
          <div id="RecaptchaField1" class="g-recaptcha"></div>
          <input type="hidden" name="register_form" value="yes">
          <input id="recaptchafor_jQuery" type="submit" value="Register Now">
        </form>
        <div class="SocialMediaAccountLogin">
          <a href="<?= $output ?>">
            <div id="facebooklogin" style="<?=$output_style?>"></div>
          </a>
          <a href="<?=$_SESSION['gpoutput'] ?>">
            <div id="googlelogin" style="<?=$gpoutput_style?>"></div>
          </a>
          <a href="<?= $linkedin_output ?>">
            <div id="LinkedInLogin" style="<?=$linkedin_style?>"></div>
          </a>
        </div>
        <div class="OR"></div>
      </div>
      <!-- ContactRegistration -->
    <div class="ContactUs">
      <div class="CloseButton" onclick="hide_ContactUs_Form()"></div>
      <div class="AboutUs">
        <div class="AboutUsInfo">
          <p>
            <h1>Our Vision</h1>
            <p>
              To be a key contributor and enabler of innovation and progress of our community both locally and globally through support, development and integration of intelligent products and services as well as through our philanthropic initiatives.
            </p>
            <h1>iBzNets - Intelligent Business Networks</h1>
            <p>
              We are technology business based in Sydney dedicated to developing innovative products that assist our clients in transforming their ideas into reality, by empowering them with the tools and support needed for success.
              Our aim is to provide the framework to build a vibrant community of startups, makers and buyers.
            </p>
            <h2>
              Our current offerings:
            </h2>
            <h1>
              Launchpad
            </h1>
            <p>
              A social crowdfunding platform to facilitate the launch of new products and services through crowdfunding campaigns and supporting project tools.
            </p>
            <h1>
              iBzaar
            </h1>
            <p>
              An agile digital marketplace for products and services, allowing seamless transition from campaign to market for our Launchpad clients.
            </p>
          </p>
        </div>
      </div>
      <form id="selectform" class="ContactRegistration" action="php/email1.php" method="post">
        <input id="fullname_for_contactus" placeholder="Full Name" type="text" name="First Name">
        <input id="email_for_contactus" placeholder="Email" type="email" name="Email Id">
        <input id="subject_for_contactus" placeholder="Subject of the message" type="text" name="Subject">
        <input id="message_for_contactus" placeholder="Message" type="textarea" name="Message">
        <div id="RecaptchaField2" class="g-recaptchaContactUs"></div>
        <input id="recaptchafor_jQueryv2" type="submit" value="Send">
      </form>
    </div>
<div id='ss_menu' >
  <div> <a href="https://www.facebook.com/Intelligent-Business-Networks-1513103602070005/"><i class="fa fa-facebook-official"></i> </a></div>
    <div><a href="https://twitter.com/iBzNets"> <i class="fa fa-twitter"></i></a> </div>
  <div> <a href="https://www.linkedin.com/company/ibznets"><i class="fa fa-linkedin"></i></a> </div>
  <div> <a href="https://au.pinterest.com/ibznets/"><i class="fa fa-pinterest"></i> </a></div>
  <div class='menu'>
    <div class='share' id='ss_toggle' data-rot='180'>
      <div class='circle'></div>
      <div class='bar'></div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(ev) {
  var toggle = $('#ss_toggle');
  var menu = $('#ss_menu');
  var rot;

  $('#ss_toggle').on('click', function(ev) {
    rot = parseInt($(this).data('rot')) - 180;
    menu.css('transform', 'rotate(' + rot + 'deg)');
    menu.css('webkitTransform', 'rotate(' + rot + 'deg )');
    if ((rot / 180) % 2 == 0) {
      //Moving in
      toggle.parent().addClass('ss_active');
      toggle.addClass('close');
    } else {
      //Moving Out
      toggle.parent().removeClass('ss_active');
      toggle.removeClass('close');
    }
    $(this).data('rot', rot);
  });

  menu.on('transitionend webkitTransitionEnd oTransitionEnd', function() {
    if ((rot / 180) % 2 == 0) {
      $('#ss_menu div i').addClass('ss_animate');
    } else {
      $('#ss_menu div i').removeClass('ss_animate');
    }
  });

});
</script>


        <!------------------------------------------------------------Social Icons--------------------------------------------------------->

<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true" style="position:absolute; z-index:9999;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><a name="terms"></a>Privacy Policy</h3>
            </div>

            <div class="modal-body">
                <div style="border: 1px solid #e5e5e5; height: 600px; overflow: auto; padding: 10px;">
                <p><b>iBN </b></p>
           <p>iBN knows that you care how information about you is used and shared, and we appreciate your trust that we will do so carefully and sensibly. This notice describes the privacy policy of iBN.<b>By visiting our website(ibznets.com/ iBzaar.com), you are accepting the practices described in this Privacy Notice.</b>
                        <br>
                        <a href="#informationcontrollers"><li>Personal information controllers</li></a>
                      <a href="#personal">  <li>Personal information about customers that  iBN gathers</li></a>
                       <a href="#cookies"> <li>Cookies </li></a>
                      <a href="#sharinginfo">  <li>Does iBN share the information it receives?</li></a>
                        <a href="#safety"><li>How safe is the information provided by me?</li></a>
                       <a href="#thirdparty"> <li>Third- party advertisers and links to other websites</li></a>
                       <a href="#access"><li>Access of information by me</li></a>
                      <a href="#choices"> <li>Choices provided </li></a>
                      <a href="#notices">  <li>Conditions of use, notices and revisions</li></a>
                    </p>

                 <a name="informationcontrollers"></a>   <p><b>Personal information controllers</b>

                    <br>Any personal information provided to or gathered by iBN is controlled by iBN, Level 35, Tower One, International Towers,100 Barangaroo Avenue, Sydney, NSW, 2000, Australia.</p>

                 <a name="personal"></a>   <p><b>Personal information about customers that iBN gathers</b></p>
                    <p>The information we learn from customers helps us personalize and continually improve your experience at ibznets.com. We use the information to handle orders, deliver products and services, process payments, communicate with you about orders, products, services and promotional offers, update our records and generally maintain your accounts with us, display content such as customer reviews and recommend merchandise and services that might be of interest to you. We may also use this information to improve our platform, prevent or detect fraud or abuses of our website and enable third parties to carry out technical, logistical or other functions on our behalf. <br>
                        Here are the types of information we gather
                        <li><b> Information You Give Us:</b> We receive and store any information you enter on our Web site or give us in any other way. You can choose not to provide certain information, but then you might not be able to take advantage of many of our features. We use the information that you provide for such purposes as responding to your requests and communicating with you.
                        </li><br>

                        <li><b>Automatic Information: </b> We receive and store certain types of information whenever you interact with us through cookies when your web browser accesses iBN sites or advertisements and other content served by or on behalf of iBN on other websites.
                        </li><br>

                        <li><b>Mobile:</b> When you download or use apps created by iBN, we may receive information about your location and your mobile device, including a unique identifier for your device. We may use this information to provide you with location-based services, such as advertising, search results, and other personalized content. Most mobile devices allow you to turn off location services.
                        </li><br>

                        <li><b>E-mail Communications:</b> To help us make e-mails more useful and interesting, we often receive a confirmation when you open e-mail from iBN. We also compare our customer list to lists received from other companies, in an effort to avoid sending unnecessary messages to our customers. If you do not want to receive e-mail or other mail from us, you can adjust your communication preferences.
                        </li><br>

                        <li>
                            <b>Information from Other Sources:</b>We might receive information about you from other sources and add it to our account information</li>
                        </p>

                    <br>

                   <a name="cookies"></a> <p><b>Cookies</b><br>In order to enable our systems to recognize your device and to provide features to you, we use cookies. Visiting our website with your browser settings adjusted to accept cookies or using progressive web apps or other software tells us that you want to use our services and that you consent to our use of cookies and other technologies to provide them to you as described here. See below for information on how to modify the settings in your device to notify you when you receive a new cookie and disable cookies altogether.

                    </p>

                 <a href="#usingcookies"><li>How to use cookies</li></a>
                   <a href="#browsersettings"> <li>Browser Settings and cookies</li></a>
                    <a href="#internetadvert"><li>Internet Advertising </li></a>
                    <br>
                   <a name="usingcookies"></a> <p><b>How to use cookies</b><br> Cookies are unique identifiers that we transfer to your device to enable our systems to recognize your device and to provide certain personalised features, Recommended for you, and interest-based advertisements on other Web sites. Any reference to "sites" of iBN includes our website, progressive web apps or other software.
                    Cookies allow you to take advantage of some of our essential features. For instance, if you block or otherwise reject our cookies, you might not be able to access our services in its full capability.
                    </p>

                   <a name="browsersettings"></a> <p><b>Browser Settings and cookies</b><br>
                    The Help feature on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie and how to disable cookies altogether. Additionally, you can disable or delete similar data used by browser add-ons, such as Flash cookies, by changing the add-on's settings or visiting the website of its manufacturer.

                    </p>

                  <a name="internetadvert"></a>  <p><b>Internet Advertising </b><br>iBN may display interest-based advertising using information you make available to us when you interact with certain of our sites, content, or services. Interest-based ads, also sometimes referred to as personalized or targeted ads, are displayed to you based on information from activities such as purchasing on our sites, use of devices, web-apps or software, visiting sites that contain iBN content or ads, interacting with iBN tools etc. Like other online ad networks, we use cookies and other technologies (collectively, "cookies"). Cookies enable us to learn about what ads you see, what ads you click, and other actions you take on our site and other sites. This allows us to provide you with more useful and relevant ads. For example, if we know what ads you are shown we can be careful not to show you the same ones repeatedly.
                    iBN does not provide any personal information to advertisers or to third party sites that display interest-based ads. However, advertisers and other third-parties (including the ad networks, ad-serving companies, and other service providers they may use) may assume that users who interact with or click on a personalized ad or content are part of the group that the ad or content is directed towards Also, some third-parties may provide us information about you (such as demographic information or sites where you have been shown ads) from offline and online sources that we may use to provide you more relevant and useful advertising.
                    Third party advertisers or advertising companies working on their behalf sometimes use technology to serve the advertisements that appear on our site directly to your browser. They automatically receive your IP address when this happens. They may also use cookies to measure the effectiveness of their advertisements and to personalize advertising content. We do not have access to or control over cookies or other features that advertisers and third party sites may use, and the information practices of these advertisers and third party websites are not covered by our Privacy policy or the cookies segment of the Privacy policy under the Terms & Conditions. Please contact them directly for more information about their privacy practices.
                    </p>

                   <a name="sharinginfo"></a> <p><b>Does iBN share the information it receives?</b><br>
                    Information about our customers is an important part of our business and we are not in the business of selling it to others. iBN shares customer information only as described below and with ibznets.com and other platforms of iBN which iBN controls and that are either subject to this Privacy policy or follow practices at least as protective as those described in this Privacy policy.</p>

                    <p><b>Third-Party Service Providers:</b><br>We may employ other companies and individuals to perform functions on our behalf. Examples include fulfilling orders, delivering packages, sending postal mail and e-mail, removing repetitive information from customer lists, analyzing data, providing marketing assistance, providing search results and links (including paid listings and links), processing credit card payments and providing customer service. They have access to personal information needed to perform their functions, but may not use it for other purposes. Further, they must process the personal information in accordance with this Privacy policy and as permitted by applicable data protection laws.</p>

                    <p><b>Promotional Offers: </b><br>Sometimes we send offers to selected groups of customers on behalf of other businesses. When we do this, we do not give that business your name and address. If you do not want to receive such offers, you can adjust your preferences of communication.
                    </p>

                    <p><b>Protection of iBN and Others: </b><br>We release account and other personal information when we believe release is appropriate to comply with the law; enforce or apply our Conditions of Use and other agreements; or protect the rights, property or safety of iBN, our users or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction. Obviously, however, this does not include selling, renting, sharing or otherwise disclosing personally identifiable information from customers for commercial purposes in a way that is contrary to the commitments made in this Privacy policy.
                    </p>

                    <p><b>With Your Consent:</b><br>Other than as set out above, you will receive notice when information about you might go to third parties, and you will have an opportunity to choose not to share the information. <br>Whenever we transfer personal information, we will ensure that the information is transferred in accordance with this Privacy policy and as permitted by the applicable laws on data protection.
                    </p>

                 <a name="safety"></a>   <p><b>How safe is the information provided by me?</b></p>
                    <li>We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you input.</li>
                    <li>We reveal only the last four digits of your credit card numbers when confirming an order. Of course, we transmit the entire credit card number to the appropriate credit card company during order processing, only after applying appropriate level of data encryption to protect and secure the information during processing & in-transit.
                    </li>
                    <li>We maintain physical, electronic and procedural safeguards in connection with the collection, storage and disclosure of personally identifiable customer information. Our security procedures mean that we may occasionally request proof of identity before we disclose personal information to you.
                    </li>
                    <li>It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer.
                    </li>

                    <br>
                   <a name="thirdparty"> </a> <p><b>Third- party advertisers and links to other websites</b><br>Our site may include third-party advertising and links to other Web sites. For more information about third-party advertising at iBN,  including personalized or interest-based ads, please read our <a href="#cookies"> Cookies</a> and <a href="#internetadvert">Internet advertising segment.</a>
                    </p>

                    <a name="access"> </a><p><b>Access of information by me</b><br>iBN gives you access to a broad range of information about your account and your interactions with iBN for the limited purpose of viewing and, in certain cases, updating that information.
                    </p>

                   <a name="choices"></a><p><b>Choices provided</b></p>
                    <li>As described earlier, you can always choose not to provide information, even though it might be needed to make a purchase or to take advantage of such iBN features as your Profile and reviews of other customers.
                    </li>
                    <li>You can add or update certain information on pages such as those referenced in the <a href="#access"> "Access of information by me" </a> section. When you update information, we usually keep a copy of the prior version for our records.
                    </li>
                    <li>If you do not want to receive e-mails or other mail from us, you can adjust your communication preferences. (If you do not want to receive <a href="#terms">Terms and conditions</a> and other legal policies from us, such as this Privacy policy, those notices will still govern your use of ibznets.com and it is your responsibility to review them for changes.)
                    </li>
                    <li>The Help feature on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie or how to disable cookies altogether. Additionally, you can disable or delete similar data used by browser add-ons, such as Flash cookies, by changing the add-on's settings or visiting the website of its manufacturer. However, because cookies allow you to take advantage of some of our essential features, we recommend that you leave them turned on. For instance, if you block or otherwise reject our cookies, you may not be able to proceed to Checkout, or use any of our services that require you to Sign in.
                    </li>

                    <br>
                 <a name="notices"></a>   <p><b>Conditions of use, notices and revisions</b><br>If you have any concern about privacy at iBN or any disputes related to privacy, please e-mail us with a thorough description and we will try to resolve the issue for you. We may e-mail periodic reminders of our notices and conditions, unless you have instructed us not to, but you should check our website frequently to see recent changes. Unless stated otherwise, our current Privacy policy applies to all information that we have about you and your account. We stand behind the promises we make, however, and will never materially change our policies and practices to make them less protective of customer information collected in the past without the consent of affected customers.</p>

            </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="agreeButton" data-dismiss="modal">OK</button>

            </div>
        </div>
    </div>
</div>
<!-- End bottom right corner -->
    <!-- JQuery Attachment -->
    <script src="javascript/code.js"></script>
</body>

</html>

