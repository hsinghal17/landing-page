<?php
        if(session_status() == PHP_SESSION_NONE)
        {
	      session_start();
        }
	session_destroy();
	$_SESSION = array();
	$cookie_time = (3600 * 24 * 30);
	setcookie("Firstname","",time() - $cookie_time);
	//setcookie("Lastname","",$EmailId, time() - $cookie_time);
	setcookie("Email","", time() -$cookie_time);
	require_once 'fbConfig.php';

// Remove access token from session
unset($_SESSION['facebook_access_token']);

// Remove user data from session
unset($_SESSION['userData']);

unset($_SESSION);
include_once 'gpConfig.php';

//Unset token and user data from session
//unset($_SESSION['token']);
//unset($_SESSION['userData']);

//Reset OAuth access token
$gClient->revokeToken();

/* Start add LinkedIn */
//unset($_SESSION['loggedin']);
/* At line of code for LinkedIn */

//Destroy entire session
session_destroy();
header("location:index.php");
exit;
	
?>
